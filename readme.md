## lib - Angular

**Initialization Project**

- ```git clone <link>```

- ```npm install```

- ```Use npm prepublish for run lint, clean dist folder and build lib```

**Lib contains:**

**Directives**

 - ng-hide
 - ng-show
 - ng-if
 - ng-bind
 - ng-model
 - ng-init
 - ng-click
 - ng-repeat
 - ng-change
 - ng-src
 - ng-file
 - ng-select


**Filters**
 
 - lowercase
 - uppercase
 - toTag



