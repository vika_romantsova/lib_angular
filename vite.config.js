import { defineConfig } from 'vite';

const path = require('path');

export default defineConfig({
  build: {
    lib: {
      entry: path.resolve(__dirname, 'lib/angular.js'),
      name: 'my-angular',
      fileName: 'my-angular',
      formats: ['es', 'cjs']
    },

    outDir: path.resolve(__dirname, 'dist'),
    sourcemap: true,
    emptyOutDir: true
  }
});
