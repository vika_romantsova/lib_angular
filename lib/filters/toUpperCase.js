export default function uppercase(value) {
  let result = value;

  value.replace(/([A-z]+)/, (match, p1) => {
    result = value.replace(match, p1[0].toUpperCase() + p1.slice(1).toLowerCase());
  });
  return result;
}
