import uppercase from './toUpperCase.js';
import lowercase from './toLowerCase.js';
import totag from './toTag.js';

export {
  uppercase,
  lowercase,
  totag
};
