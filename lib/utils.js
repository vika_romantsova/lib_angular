function extractFunctionArguments(func) {
  const args = func.toString().match(/\((.*)\)/)[1];

  return args.split(/\s*,\s*/);
}

export default class Utils {
  static capitalize(str) {
    const replaceCb = (_, p1) => p1.toUpperCase();
    const regExp = /[\W_]([A-z])/g;

    return str.toLowerCase().replace(regExp, replaceCb);
  }

  static setDependencies(args) {
    let fn = args;

    if (Array.isArray(args)) {
      fn = args.pop();
      fn.dependencies = args;
    } else if ((/inject/).test(fn)) {
      fn.dependencies = extractFunctionArguments(fn);
    }

    if (!fn.dependencies) {
      fn.dependencies = [];
    }

    return fn;
  }
}
