/* eslint-disable no-setter-return */
/* eslint-disable no-empty */
/* eslint-disable no-new-func */
import Utils from './utils.js';
import commonDirectives from './directives.js';
import * as defaultFilters from './filters/index.js';
import * as defaultServices from './services/index.js';

class InterpolationParse {
  #filters = { ...defaultFilters };
  constructor(rootScope) {
    this.rootScope = rootScope;
    this.regExp = /{{([^}{]+)}}/g;
  }

  #processor(templateString) {
    let result = templateString;
    templateString.replace(this.regExp, (match, expressionAsString) => {
      const options = expressionAsString.split('|').map(item => item.trim());

      if (options.length < 2) {
        result = result.replace(match, this.rootScope.eval(expressionAsString));
        return;
      }

      const [expression, ...interpolationFilters] = options;


      const temp = interpolationFilters.reduce(
        (current, filter) => {
          if (!this.#filters[filter]) {
            throw new Error(`Incorrect filter name "${filter}" in interpolation ${expression}`);
          }
          return this.#filters[filter](current);
        },
        this.rootScope.eval(expression)
      );

      result = result.replace(match, temp);
    });
    return result;
  }

  registerNodeText(node) {
    try {
      for (const childNode of node.childNodes) {
        if (childNode.nodeType === 3 && this.regExp.test(childNode.textContent)) {
          childNode.interpolationTemplate = childNode.nodeValue;
          childNode.nodeValue = this.#processor(childNode.interpolationTemplate);
        }
      }
    } catch (err) {}
  }

  parseNodeText(node) {
    for (const childNode of node.childNodes) {
      if (childNode.nodeType === 3 && this.regExp.test(childNode.textContent)) {
        childNode.nodeValue = this.#processor(childNode.interpolationTemplate);
      }
    }
  }

  interpolateApp() {
    const collection = document.querySelector('[ng-app]').querySelectorAll('*');
    collection.forEach(this.parseNodeText.bind(this));
  }
}

class SmallAngular {
  rootScope = window;
  directives = { ...commonDirectives };
  components = {};
  #watchers = [];
  #interpolation = null;
  configs = [];
  services = {};
  controllers = {};

  constructor() {
    this.rootScope.controllersData = {};
    this.#interpolation = new InterpolationParse(this.rootScope);

    for (const serv in defaultServices) {
      this.service(serv, defaultServices[serv]);
    }
    this.#initPublicProperties();
    setTimeout(() => {
      this.configs.forEach(fn => {
        const funcArgs = this.#getDep(fn.dependencies);
        fn(...funcArgs);
      });
      this.#bootstrap();
    }, 150);
  }

  #eval(expression, scope = this.rootScope) {
    const expressionFunc = new Function(...Object.keys(scope), `return ${expression}`);

    return expressionFunc(...Object.values(scope));
  }

  #decomposeAttributes({ attributes }) {
    const directiveAttributes = {};
    const commonAttributes = {};

    for (const { name, value } of attributes) {
      const directiveName = Utils.capitalize(name);

      if (directiveName in this.directives) {
        directiveAttributes[directiveName] = value;
      } else {
        commonAttributes[name] = value;
      }
    }

    return {
      directiveAttributes,
      commonAttributes
    };
  }

  #compile(node, scope = this.rootScope) {
    this.#interpolation.registerNodeText(node);
    const tag = Utils.capitalize(node.tagName);
    const component = this.components[tag];

    if (component) {
      const { template, controller, controllerAs, link } = component();


      if (template) {
        node.innerHTML = template;
      }

      const Controler = this.controllers[controller];

      const controlDeps = this.#getDep(Controler.dependencies);
      const controlInst = new Controler(...controlDeps);

      if (controllerAs) {
        this.rootScope[controllerAs] = controlInst;
      }


      if (link) {
        link(scope, node);
      }

      this.#compileDeep(node, scope);
      return;
    }


    const {
      directiveAttributes,
      commonAttributes
    } = this.#decomposeAttributes(node);

    for (const directiveKey in directiveAttributes) {
      this.directives[directiveKey]().link(scope, node, commonAttributes);
    }
  }

  #callEvalWithNodeScope(expression, node) {
    const currentScope = this.#getScope(node);

    return this.#eval(expression, currentScope);
  }

  #getScope(node) {
    const controllerName = node.getAttribute('ng-controller-data') || node.getAttribute('ng-controller');

    if (!this.rootScope.controllersData[controllerName]) {
      return this.rootScope;
    }

    const controllerScope = this.rootScope.controllersData[controllerName];
    const temproraryData = {};
    const { rootScope } = this;

    for (const key in controllerScope) {
      let mainValue = controllerScope[key];
      Object.defineProperty(temproraryData, key, {
        enumerable: true,
        get() {
          return mainValue;
        },
        set(val) {
          controllerScope[key] = val;
          mainValue = val;
          return mainValue;
        }
      });
    }

    for (const key in rootScope) {
      let mainValue = rootScope[key];

      if (temproraryData[key] === undefined) {
        Object.defineProperty(temproraryData, key, {
          enumerable: true,
          get() {
            return mainValue;
          },
          set(val) {
            mainValue = val;
            rootScope[key] = mainValue;

            return mainValue;
          }
        });
      }
    }

    return temproraryData;
  }

  #compileDeep(node, scope = this.rootScope) {
    const collection = node.querySelectorAll('*');
    collection.forEach(node => this.#compile(node, scope));
  }

  #apply() {
    this.#interpolation.interpolateApp();
    this.#watchers.forEach(({ node, cb }) => {
      cb();
    });
  }

  #watch(node, cb) {
    this.#watchers.push({ node, cb });
  }

  #bootstrap(node = document.querySelector('[ng-app]')) {
    this.#compileDeep(node);
  }

  #getDep(names, scope = this.rootScope) {
    return names.map(name => {
      if (name === '$rootScope') {
        return this.rootScope;
      }

      if (name === 'scope') {
        return scope;
      }

      return this.services[name];
    });
  }

  controller(name, cb) {
    const funcWithDeps = Utils.setDependencies(cb);
    this.controllers[name] = funcWithDeps;

    return this;
  }

  component(name, cb) {
    this.components[name] = cb;

    return this;
  }

  constant(name, value) {
    this.services[name] = value;

    return this;
  }

  service(name, fn) {
    const funcWithDeps = Utils.setDependencies(fn);
    const funcArgs = this.#getDep(funcWithDeps.dependencies);

    this.services[name] = funcWithDeps(...funcArgs);

    return this;
  }


  config(fn) {
    const funcWithDeps = Utils.setDependencies(fn);
    this.configs.push(funcWithDeps);

    return this;
  }

  module = appName => {
    this.appName = appName;

    return this;
  };

  applyAsync() {
    setTimeout(this.#apply.bind(this));
  }

  get bootstrap() {
    return this.#bootstrap.bind(this);
  }

  #initPublicProperties() {
    Object.assign(this.rootScope, {
      bootstrap: this.bootstrap.bind(this),
      $watch: this.#watch.bind(this),
      $apply: this.#apply.bind(this),
      $compile: this.#compile.bind(this),
      $compileDeep: this.#compileDeep.bind(this),
      $eval: this.#eval.bind(this),
      $getScope: this.#getScope.bind(this),
      $callEvalWithNodeScope: this.#callEvalWithNodeScope.bind(this),
      $applyAsync: this.applyAsync.bind(this)
    });
  }
}

const angular = new SmallAngular();
window.angular = angular;

export default angular;
