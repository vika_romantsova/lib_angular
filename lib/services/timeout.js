export default function $timeout(rootScope) {
  return function(cb, delay) {
    setTimeout(() => {
      cb();
      rootScope.$applyAsync();
    }, delay);
  };
}

$timeout.dependencies = ['$rootScope'];
