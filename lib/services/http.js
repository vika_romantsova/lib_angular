export default function http($rootScope) {
  const resultTypeHandlers = {
    'blob': res => res.blob(),
    'json': res => res.json(),
    'text': res => res.text()
  };

  class HttpRequest {
    constructor(url) {
      this.url = url;
    }

    get(path, config) {
      return this.request({ ...config, path, method: 'GET' });
    }

    post(path, data, config) {
      return this.request({ ...config, path, method: 'POST', data });
    }

    delete(path, config) {
      return this.request({ ...config, path, method: 'DELETE' });
    }

    request(config) {
      const { path, method, responseType = 'json', data, headers } = config;
      const url = new URL(path, this.url);

      return fetch(url.href, { method, body: data, headers })
        .then(res => {
          $rootScope.$applyAsync();

          if (!res.ok) {
            throw new Error();
          }

          return resultTypeHandlers[responseType](res);
        });
    }
  }

  return new HttpRequest();
}

http.dependencies = ['$rootScope'];
