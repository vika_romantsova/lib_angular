const ngHide = () => ({
  link(scope, node) {
    const directiveValue = node.getAttribute('ng-hide');

    const watcher = () => {
      node.hidden = scope.eval(directiveValue);
    };

    scope.$watch(node, watcher);
    watcher();
  }
});

const ngShow = () => ({
  link(scope, node) {
    const directiveValue = node.getAttribute('ng-show');

    const watcher = () => {
      node.hidden = !scope.eval(directiveValue);
    };

    scope.$watch(node, watcher);
    watcher();
  }
});

const ngIf = () => ({
  link(scope, node) {
    const directiveValue = node.getAttribute('ng-if');
    const createComment = document.createComment(`ng-if: ${directiveValue}`);
    node.before(createComment);
    node.removeAttribute('ng-if');

    createComment.mainElement = node.cloneNode(true);
    const { parentNode } = node;
    parentNode.removeChild(node);

    let newElement = null;

    const watcher = () => {
      if (newElement) {
        parentNode.removeChild(newElement);
        newElement = null;
      }

      const isShow = Boolean(scope.eval(directiveValue));

      if (!isShow) {
        return;
      }

      newElement = createComment.mainElement.cloneNode(true);
      scope.$compileDeep(newElement);
      createComment.after(newElement);
    };

    scope.$watch(parentNode, watcher);
    watcher();
  }
});


const ngBind = () => ({
  link(scope, node) {
    const directiveValue = node.getAttribute('ng-bind');
    const watcher = () => {
      node.innerText = scope.$callEvalWithNodeScope(directiveValue, node);
    };

    scope.$watch(node, watcher);
    watcher();
  }
});

const ngModel = () => ({
  link: (scope, node, attrs) => {
    const variable = node.getAttribute('ng-model');
    const key = attrs.type === 'checkbox' ? 'checked' : 'value';

    const setVariable = () => {
      if (key === 'value') {
        scope.eval(`${variable} = '${node[key]}'`);
        return;
      }
      scope.eval(`${variable} = ${node[key]}`);
    };

    if (!scope[variable]) {
      setVariable();
    }

    const setInputValue = () => {
      node[key] = scope.eval(variable);
    };

    setInputValue();
    scope.$watch(variable, setInputValue);

    node.addEventListener('input', ({ target }) => {
      setVariable();
      scope.$apply();
    });
  }
});

const ngInit = () => ({
  link(scope, node) {
    const directiveValue = node.getAttribute('ng-init');
    scope.eval(directiveValue);
  }
});

const ngClick = () => ({
  link(scope, node) {
    const directiveValue = node.getAttribute('ng-click');
    node.onclick = () => {
      const ngRepValue = node.ngRepeatValue ?? {};
      scope.$eval(directiveValue, { ...scope, ...ngRepValue });
      scope.$apply();
    };
  }
});

const ngRepeat = () => ({
  link(scope, node) {
    const directiveValue = node.getAttribute('ng-repeat');
    const [item, array] = directiveValue.split('of').map(item => item.trim());
    const { parentNode } = node;
    parentNode.repeatElem = node.cloneNode(true);
    parentNode.removeChild(node);
    parentNode.repeatElem.removeAttribute('ng-repeat');

    const watcher = () => {
      const initialArray = scope.eval(array);
      const shouldBeDeletedItems = parentNode.querySelectorAll('[ng-delete]');

      if (shouldBeDeletedItems.length) {
        shouldBeDeletedItems.forEach(item => {
          parentNode.removeChild(item);
        });
      }

      for (const value of initialArray) {
        const currentElement = parentNode.repeatElem.cloneNode(true);
        currentElement.setAttribute('ng-delete', '');
        scope[item] = value;
        currentElement.querySelectorAll('*').forEach(element => {
          element.ngRepeatValue = { [item]: value };
        });
        scope.$compileDeep(currentElement);
        parentNode.appendChild(currentElement);
      }
    };

    scope.$watch(parentNode, watcher);
    watcher();
  }
});

const ngChange = () => ({
  link: (scope, node) => {
    const variable = node.getAttribute('ng-change');

    node.addEventListener('change', () => {
      const value = node['value'].split('\\')[2];
      scope.eval(`${variable} = '${value}'`);
      scope.$apply();
    });
  }
});

const ngController = () => ({
  link(scope, node) {
    const directiveValue = node.getAttribute('ng-controller');

    const collection = node.querySelectorAll('*');
    collection.forEach(element => {
      const attrValue = node.getAttribute('ng-controller-data');

      if (!attrValue) {
        element.setAttribute('ng-controller-data', directiveValue);
      }
    });

    const watcher = () => {
      scope.$compileDeep(node, scope.$getScope(node));
    };

    scope.$watch(node, watcher);
    setTimeout(() => watcher());
  }
});

const ngSrc = () => ({
  link(scope, node) {
    const directiveValue = node.getAttribute('ng-src');

    const watcher = () => {
      if (!scope.eval(directiveValue)) {
        return;
      }
      node.src = scope.eval(directiveValue);
    };

    scope.$watch(node, watcher);
  }
});

const ngFile = () => ({
  link: (scope, node) => {
    const [ctrl, variable] = node.getAttribute('ng-file').split('.');

    node.addEventListener('change', ({ target: { files } }) => {
      scope[ctrl][variable] = files[0];
    });
  }
});

const ngSelect = () => ({
  link: (scope, node) => {
    const variable = node.getAttribute('ng-select');
    const nodeText = node.textContent;

    const selectText = () => {
      node.textContent = nodeText;
      const value = scope.eval(variable);
      const textNode = node.firstChild;
      const textIndex = textNode.nodeValue.indexOf(value);

      if (textIndex >= 0) {
        const range = new Range();
        range.setStart(textNode, textIndex);
        range.setEnd(textNode, textIndex + value.length);
        const select = document.createElement('span');
        select.className = 'select';
        range.surroundContents(select);
      }
    };

    selectText();
    scope.$watch('text-select', selectText);
  }
});


export default {
  ngHide,
  ngShow,
  ngBind,
  ngModel,
  ngInit,
  ngClick,
  ngRepeat,
  ngIf,
  ngController,
  ngSrc,
  ngFile,
  ngSelect,
  ngChange
};
